import sys
import subprocess

template_file = open("../blog_template.html", "r")
template = template_file.read()
template_file.close()

args = sys.argv
md_file = args[1]
html = subprocess.run(["pandoc", md_file], capture_output=True).stdout


template = template.replace("### CONTENT ###", html.decode("utf-8"))

out_file_name = md_file
out_file_name = out_file_name.replace(".md", ".html")
out_file = open(f"../{out_file_name}", "w")
out_file.write(template)
out_file.close()
